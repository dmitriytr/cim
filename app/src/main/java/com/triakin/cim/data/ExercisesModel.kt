package com.triakin.cim.data

data class ExercisesModel(
    var x: Int = 0,
    var y: Int = 0,
    var answer: Int = 0,
    var operator: String = PLUS
) {
    companion object {
        const val PLUS = "+"
        const val MINUS = "-"
        const val MULTIPLY = "*"
        const val DIVIDE = "/"
    }
}

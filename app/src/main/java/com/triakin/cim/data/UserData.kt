package com.triakin.cim.data

data class UserData(
    var name: String = "",
    var email: String = "",
    var maxNumber: Int = 10
)

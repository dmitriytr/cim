package com.triakin.cim.data.repository

import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.triakin.cim.data.UserData
import kotlinx.coroutines.tasks.await

class UsersRep(private val db: FirebaseFirestore) {

    companion object {
        const val TAG = "UsersRep_firestore"
        const val COLLECTION_PATH = "users"
    }

    fun updateUser(user: UserData, uid: String) {
        db.collection(COLLECTION_PATH).document(uid)
            .set(user)
            .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully written!") }
            .addOnFailureListener { e -> Log.d(TAG, "Error writing document", e) }
    }

    suspend fun getUser(uid: String): UserData {
        return db.collection(COLLECTION_PATH).document(uid).get().await()
            .toObject(UserData::class.java) ?: UserData()
    }

    suspend fun isExist(uid: String): Boolean {
        return db.collection(COLLECTION_PATH).document(uid).get().await().exists()
    }

}
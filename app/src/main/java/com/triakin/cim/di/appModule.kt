package com.triakin.cim.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.triakin.cim.data.repository.UsersRep
import com.triakin.cim.domain.MainUseCase
import com.triakin.cim.domain.SettingsUseCase
import com.triakin.cim.presentation.main.MainViewModel
import com.triakin.cim.presentation.settings.SettingsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single { FirebaseAuth.getInstance() }
    single { Firebase.firestore }
    single { UsersRep(get()) }
    single { MainUseCase(get(), get()) }
    single { SettingsUseCase(get(), get()) }

    viewModel { MainViewModel(get()) }
    viewModel { SettingsViewModel(get()) }

}
package com.triakin.cim.domain

import com.google.firebase.auth.FirebaseAuth
import com.triakin.cim.data.ExercisesModel
import com.triakin.cim.data.UserData
import com.triakin.cim.data.repository.UsersRep

class MainUseCase(rep: UsersRep, auth: FirebaseAuth) : UseCase(rep, auth) {

    private var maxNumber = 10

    override fun applyUserData(user: UserData) {
        maxNumber = user.maxNumber
    }

    fun checkAnswer(value: Int): Boolean = (exModel.answer == value)

    fun generatePlusExercise(): ExercisesModel {
        exModel.x = (1..maxNumber).random()
        exModel.y = (1..maxNumber).random()
        exModel.answer = exModel.x + exModel.y
        exModel.operator = ExercisesModel.PLUS
        return exModel
    }

    fun generateMinusExercise(): ExercisesModel {
        exModel.x = (2..maxNumber).random()
        do {
            exModel.y = (1..exModel.x).random()
        } while (exModel.x == exModel.y)
        exModel.answer = exModel.x - exModel.y
        exModel.operator = ExercisesModel.MINUS
        return exModel
    }

    fun generateMultiplyExercise(): ExercisesModel {
        exModel.x = (2..maxNumber).random()
        exModel.y = (2..maxNumber).random()
        exModel.answer = exModel.x * exModel.y
        exModel.operator = ExercisesModel.MULTIPLY
        return exModel
    }

    fun generateDivideExercise(): ExercisesModel {
        do {
            do {
                exModel.x = (4..maxNumber).random()
                exModel.y = (2..exModel.x).random()
            } while (exModel.x % exModel.y != 0)
        } while (exModel.x == exModel.y)
        exModel.answer = exModel.x / exModel.y
        exModel.operator = ExercisesModel.DIVIDE
        return exModel
    }
}
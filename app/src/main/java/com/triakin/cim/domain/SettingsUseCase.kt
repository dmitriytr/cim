package com.triakin.cim.domain

import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.triakin.cim.data.UserData
import com.triakin.cim.data.repository.UsersRep

class SettingsUseCase(rep: UsersRep, auth: FirebaseAuth) : UseCase(rep, auth) {

    val email = MutableLiveData("")
    val maxNumber = MutableLiveData(10)

    override fun applyUserData(user: UserData) {
        email.value = user.email
        maxNumber.value = user.maxNumber
    }

    suspend fun authSuccess() {
        auth.currentUser?.let {
            if (rep.isExist(it.uid)) {
                init()
            } else {
                val user = UserData(
                    it.displayName ?: "",
                    it.email ?: "",
                    maxNumber.value ?: 10
                )
                rep.updateUser(user, it.uid)
                applyUserData(user)
            }
        }
    }
}
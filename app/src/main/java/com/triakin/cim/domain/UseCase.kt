package com.triakin.cim.domain

import com.google.firebase.auth.FirebaseAuth
import com.triakin.cim.data.ExercisesModel
import com.triakin.cim.data.UserData
import com.triakin.cim.data.repository.UsersRep

abstract class UseCase(
    val rep: UsersRep,
    val auth: FirebaseAuth
) {

    protected val exModel = ExercisesModel()

    suspend fun init() {
        auth.uid?.let {
            applyUserData(rep.getUser(it))
        }
    }

    abstract fun applyUserData(user: UserData)
}
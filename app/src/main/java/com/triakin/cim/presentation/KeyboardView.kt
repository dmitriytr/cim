package com.triakin.cim.presentation

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import com.triakin.cim.R
import com.triakin.cim.util.find

class KeyboardView : FrameLayout, View.OnClickListener {

    private lateinit var mInput: EditText
    private val watcher = object : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if (s.isNotEmpty()) {
                observe.invoke(s.toString().toInt())
            }
        }

        override fun afterTextChanged(s: Editable) {}
    }
    var observe: (Int) -> Unit = {}

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    init {
        inflate(context, R.layout.keyboard, this)
        initViews()

        mInput.addTextChangedListener(watcher)
    }

    override fun onClick(v: View) {
        // handle number button click
        if (v.tag != null && "number_button" == v.tag) {
            mInput.append((v as TextView).text)
            return
        }
        when (v.id) {
            R.id.tv_key_clear -> {
                // handle clear button
                mInput.text = null
            }
            R.id.tv_key_backspace -> {
                // handle backspace button
                // delete one character
                val editable = mInput.text
                val charCount = editable.length
                if (charCount > 0) {
                    editable.delete(charCount - 1, charCount)
                }
            }
        }
    }

    fun clear() {
        mInput.text.clear()
    }

    private fun initViews() {
        mInput = find(R.id.et_input)
        find<View>(R.id.tv_key_0).setOnClickListener(this)
        find<View>(R.id.tv_key_1).setOnClickListener(this)
        find<View>(R.id.tv_key_2).setOnClickListener(this)
        find<View>(R.id.tv_key_3).setOnClickListener(this)
        find<View>(R.id.tv_key_4).setOnClickListener(this)
        find<View>(R.id.tv_key_5).setOnClickListener(this)
        find<View>(R.id.tv_key_6).setOnClickListener(this)
        find<View>(R.id.tv_key_7).setOnClickListener(this)
        find<View>(R.id.tv_key_8).setOnClickListener(this)
        find<View>(R.id.tv_key_9).setOnClickListener(this)
        find<View>(R.id.tv_key_clear).setOnClickListener(this)
        find<View>(R.id.tv_key_backspace).setOnClickListener(this)
    }

}
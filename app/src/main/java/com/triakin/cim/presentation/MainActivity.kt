package com.triakin.cim.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.triakin.cim.R
import com.triakin.cim.presentation.main.MainFragment

class MainActivity : AppCompatActivity() {

//    private lateinit var analytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

//        analytics = FirebaseAnalytics.getInstance(this)
//        analytics.logEvent("start_MainActivity", null)
        getPreferences(MODE_PRIVATE)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }
    }
}
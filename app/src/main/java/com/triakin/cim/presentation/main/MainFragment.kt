package com.triakin.cim.presentation.main

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.triakin.cim.R
import com.triakin.cim.databinding.MainFragmentBinding
import com.triakin.cim.presentation.MainActivity
import com.triakin.cim.presentation.settings.SettingsFragment
import org.koin.android.ext.android.inject

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel by inject<MainViewModel>()
    private var _binding: MainFragmentBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        _binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.vm = viewModel

        binding.keyboardView.observe = {
            if (viewModel.checkAnswer(it)) {
                binding.keyboardView.clear()
            }
        }

        prepareToolbar()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_settings, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_settings -> {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.container, SettingsFragment.newInstance())?.commitNow()
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun prepareToolbar() {
        (activity as MainActivity).supportActionBar?.let {
            it.title = getString(R.string.app_name)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
package com.triakin.cim.presentation.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.triakin.cim.domain.MainUseCase
import kotlinx.coroutines.launch

class MainViewModel(private val useCase: MainUseCase) : ViewModel() {

    private val _exercise = MutableLiveData("")
    val exercise: LiveData<String> = _exercise

    init {
        viewModelScope.launch {
            useCase.init()
            generateExercise((0..3).random())
        }
    }

    private fun generateExercise(value: Int) {
        val exModel = when (value) {
            0 -> useCase.generatePlusExercise()
            1 -> useCase.generateMinusExercise()
            2 -> useCase.generateMultiplyExercise()
            else -> useCase.generateDivideExercise()
        }
        _exercise.value = "${exModel.x} ${exModel.operator} ${exModel.y} = ?"
    }

    fun checkAnswer(value: Int): Boolean {
        return (useCase.checkAnswer(value)).apply {
            if (this) generateExercise((0..3).random())
        }
    }
}
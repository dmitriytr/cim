package com.triakin.cim.presentation.settings

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.firebase.ui.auth.data.model.FirebaseAuthUIAuthenticationResult
import com.triakin.cim.domain.SettingsUseCase
import kotlinx.coroutines.launch

class SettingsViewModel(private val useCase: SettingsUseCase) : ViewModel() {

    val email: LiveData<String> = useCase.email
    val maxNumber: LiveData<Int> = useCase.maxNumber

    init {
        viewModelScope.launch {
            useCase.init()
        }
    }

    fun setProgress(progress: Int) {
        val n = (progress + 1) * 10
        useCase.maxNumber.value = n
    }

    fun onSignInResult(result: FirebaseAuthUIAuthenticationResult) {
//        val response = result.idpResponse
        if (result.resultCode == AppCompatActivity.RESULT_OK) {
            // Successfully signed in
            viewModelScope.launch {
                useCase.authSuccess()
            }
        } else {
            // Sign in failed. If response is null the user canceled the
            // sign-in flow using the back button. Otherwise check
            // response.getError().getErrorCode() and handle the error.
        }
    }

}